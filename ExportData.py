import pyodbc
import csv

conn = pyodbc.connect('Driver={SQL Server};SERVER=localhost;DATABASE=DATAINTAL;UID=dataintal_user;PWD=Begin1961')


file = "C:\Users\gcasella\Desktop\ProcesoPythonINTrade\datasetpaises.csv"

header=[]
datos=[]
trimfechas=['01','02','03','04']
contador = 0
country = 'Null'

with open(file,'r') as f:
    lineas = f.read().splitlines()
    for i in lineas:
        if contador == 0:
            header = i.split(',')
            contador = 1
        else:
            datos = i.split(',')
            posicion = 0
            for j in datos:
                if posicion == 0:
                    country = datos[0]
                    posicion += 1
                else:
                    tipodedato = datos[posicion]
                    anioheader = header[posicion]
                    if tipodedato == 'A':
                        cursor = conn.cursor()
                        sql = """SELECT C.[PAI_ID]
                              ,[PAI_ID_SOCIO]
                              ,[COM_ANIO]
                              ,P.[PRD_PRODUCTO]
                              ,[TCO_ID]
                              ,[MED_ID]
                              ,SUM([COM_VOLUMEN]) AS VOLUMEN
                              ,SUM([COM_VALOR]) AS VALOR
                          FROM [DATAINTAL].[dbo].[COMERCIO] AS C, [DATAINTAL].[dbo].[PRODUCTOS] AS P
                          WHERE P.PRD_ID = C.PRD_ID AND C.PAI_ID = '"""+ str(country) +"""' AND COM_ANIO = """+ str(anioheader) +"""
                          GROUP BY C.[PAI_ID]
                              ,[PAI_ID_SOCIO]
                              ,[COM_ANIO]
                              ,P.[PRD_PRODUCTO]
                              ,[TCO_ID]
                              ,[MED_ID]"""
                        cursor.execute(sql)
                        row = cursor.fetchall()
                        filename = "INTrade_"+str(country)+"_"+str(anioheader)+"_A.csv"
                        with open("./Archivos/"+filename, 'wb') as f:
                            a = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
                            a.writerow(["PAI_ID", "PAI_ID_SOCIO", "COM_ANIO", "PRD_ID", "TCO_ID", "MED_ID", "VOLUMEN", "VALOR"])
                            a.writerows(row)
                        cursor.close()
                        posicion += 1
                    
                    if tipodedato == 'T':
                        for k in trimfechas:
                            cursor = conn.cursor()
                            sql = """SELECT C.[PAI_ID]
                                  ,[PAI_ID_SOCIO]
                                  ,[COM_ANIO]
                                  ,P.[PRD_PRODUCTO]
                                  ,[TCO_ID]
                                  ,[MED_ID]
                                  ,SUM([COM_VOLUMEN]) AS VOLUMEN
                                  ,SUM([COM_VALOR]) AS VALOR
                              FROM [DATAINTAL].[dbo].[COMERCIO_TRIMESTRES] AS C, [DATAINTAL].[dbo].[PRODUCTOS] AS P
                              WHERE P.PRD_ID = C.PRD_ID AND C.PAI_ID = '"""+ str(country) +"""' AND COM_ANIO = """+ str(anioheader)+str(k) +"""
                              GROUP BY C.[PAI_ID]
                                  ,[PAI_ID_SOCIO]
                                  ,[COM_ANIO]
                                  ,P.[PRD_PRODUCTO]
                                  ,[TCO_ID]
                                  ,[MED_ID]"""
                            cursor.execute(sql)
                            row = cursor.fetchall()
                            filename = "INTrade_"+str(country)+"_"+str(anioheader)+"_T.csv"
                            with open("./Archivos/"+filename, 'ab') as f:
                                first = str(k)
                                if first == '01':
                                  a = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
                                  a.writerow(["PAI_ID", "PAI_ID_SOCIO", "COM_ANIO", "PRD_ID", "TCO_ID", "MED_ID", "VOLUMEN", "VALOR"])
                                  a.writerows(row)
                                else:
                                  a = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
                            cursor.close()
                        posicion += 1

                    if tipodedato == 'N':
                        posicion += 1